/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class Usuario {
    String nome,senha;
    int idUsuario;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
    
    public boolean insert() {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;
        int codigoVenda = 0;

        String insertTableSQL = "INSERT INTO OO_UsuarioJDBC3 (ID_USUARIO,NOME , senha ) VALUES (SEQ_USUARIO.nextval,?,?)";
        

        try {
            
            String generatedColumns[] = {"ID_USUARIO"};
            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);
            
            
            ps.setString(1, getNome());
            ps.setInt(2, getIdUsuario());
            
            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
            }
            setIdUsuario(chaveGerada);
            
        } catch (SQLException e) {
           
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        } 
        
        return true;
    }
    
    
    
    public static ArrayList<Usuario> getAll() {
        String selectSQL = "SELECT * FROM OO_UsuarioJDBC3";
        
        
        ArrayList<Usuario> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st,st2 = null;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = st.executeQuery();
            
            
            while (rs.next()) {
                

                Usuario f = new Usuario();
                f.setNome(rs.getString("NOME"));
                f.setIdUsuario(rs.getInt("ID_USUARIO"));
                f.setSenha(rs.getString("senha"));
                
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta(); 
        }   

        return lista;
    }

}
