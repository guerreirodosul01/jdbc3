/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc3;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaInicialController implements Initializable {

    @FXML
    private TextField reSenhaTF;
    @FXML
    private TextField senhaTF;
    @FXML
    private TextField loginTF;
    @FXML
    private TableColumn<Usuario, Integer> idColuna;
    @FXML
    private TableColumn<Usuario, String> loginColuna;
    @FXML
    private TableColumn<Usuario, String> senhaColuna;
    @FXML
    private TextField interreseTF;
    @FXML
    private TableColumn<Interesse, Integer> idInteresseColuna;
    @FXML
    private TableColumn<Interesse, String> nomeColuna;
    
    private ObservableList<Usuario> usuariosList;
    private ObservableList<Interesse> interessesList;
    @FXML
    private TableView<Usuario> tabela;
    @FXML
    private TableView<Interesse> tabelaInteresses;
    @FXML
    private Button cadastrarInteresse;
    @FXML
    private Label mensagem;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        usuariosList = tabela.getItems();
        usuariosList.clear();
        idColuna.setCellValueFactory(new PropertyValueFactory<>("idUsuario"));      
        loginColuna.setCellValueFactory(new PropertyValueFactory<>("nome")); 
        senhaColuna.setCellValueFactory(new PropertyValueFactory<>("senha"));
        Usuario.getAll().forEach((a) -> {
            usuariosList.add(a);
        });
        interreseTF.setDisable(true);
        cadastrarInteresse.setDisable(true);
    }    

    @FXML
    private void cadastrarUsuario(ActionEvent event) {
        if(senhaTF.getText().equals(reSenhaTF.getText()) && !loginTF.getText().isEmpty()){
            Usuario a = new Usuario();
            a.setNome(loginTF.getText());
            a.setSenha(senhaTF.getText());
            a.insert();
            usuariosList.add(a);
            mensagem.setText(" ");
            
        }else{
            mensagem.setText("Preencha todos os campos corretamente");
        }
    }

    @FXML
    private void usuarioSelecionada(MouseEvent event) {
        interreseTF.setDisable(false);
        cadastrarInteresse.setDisable(false);
        interessesList = tabelaInteresses.getItems();
        interessesList.clear();
        idInteresseColuna.setCellValueFactory(new PropertyValueFactory<>("id"));      
        nomeColuna.setCellValueFactory(new PropertyValueFactory<>("nome")); 
        Interesse.getAll(tabela.getSelectionModel().getSelectedItem().getIdUsuario()).forEach((a) -> {
            interessesList.add(a);
        });
    }

    @FXML
    private void cadastrarInteresse(ActionEvent event) {
        Interesse a =  new Interesse();
        a.setNome(interreseTF.getText());
        a.setIdUsuario(tabela.getSelectionModel().getSelectedItem().getIdUsuario());
        a.insert();
        interessesList.add(a);
    }
    
}
