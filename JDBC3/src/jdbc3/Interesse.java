/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class Interesse {
    String nome;
    int id,idUsuario;

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public boolean insert() {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;
        int codigoVenda = 0;

        String insertTableSQL = "INSERT INTO OO_USUARIOINTERESSESJDBC3 (ID_INTERESSE,ID_USUARIO,NOME  ) VALUES (SEQ_USUARIOINTERESSE.nextval,?,?)";
        

        try {
            
            String generatedColumns[] = {"ID_INTERESSE"};
            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);
            
            
            ps.setString(2, getNome());
            ps.setInt(1, getIdUsuario());
            
            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
            }
            setId(chaveGerada);
            
        } catch (SQLException e) {
           
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        } 
        
        return true;
    }
    
    
    
    public static ArrayList<Interesse> getAll(int id) {
        String selectSQL = "SELECT * FROM OO_USUARIOINTERESSESJDBC3 where ID_USUARIO = ?";
        
        
        ArrayList<Interesse> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st,st2 = null;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setInt(1, id);
            
            st.executeUpdate();
            ResultSet rs = st.executeQuery();
            
            
            while (rs.next()) {
                

                Interesse f = new Interesse();
                f.setId(rs.getInt("ID_INTERESSE"));
                f.setIdUsuario(rs.getInt("ID_USUARIO"));
                f.setNome(rs.getString("NOME"));
                
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta(); 
        }   

        return lista;
    }
    
}
